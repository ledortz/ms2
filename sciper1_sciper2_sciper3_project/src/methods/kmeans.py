import numpy as np


class KMeans(object):
    """
    K-Means clustering class.

    We also use it to make prediction by attributing labels to clusters.
    """

    def __init__(self, K, max_iters=100):
        """
        Initialize the new object (see dummy_methods.py)
        and set its arguments.

        Arguments:
            K (int): number of clusters
            max_iters (int): maximum number of iterations
        """
        if K == 0:
            raise ValueError('kmeans cannot work with k = 0')
        self.centers_labels = None
        self.centers = None
        self.K = K
        self.max_iters = max_iters

    def init_centers(self, data):
        return data[np.random.permutation(data.shape[0])[:self.K]]

    def compute_distance(self, data, centers):
        return np.array([np.sqrt(np.sum(np.power(v - centers, 2), axis=1)) for i, v in enumerate(data)])

    def find_closest_cluster(self, distances):
        return np.argmin(distances, axis=1)

    def compute_centers(self, data, cluster_assignments):
        return np.array([(1 / np.sum(cluster_assignments == i)) * np.sum(data[cluster_assignments == i], axis=0) for i in np.arange(self.K)])

    def assign_labels_to_center(self, centers, cluster_assignments, true_labels):
        return [np.argmax(np.bincount(true_labels[cluster_assignments == i])) for i in range(centers.shape[0])]

    def predict_with_centers(self, data, centers, cluster_center_label):
        distances = self.compute_distance(data, centers)
        cluster_assignments = self.find_closest_cluster(distances)
        new_labels = np.array([cluster_center_label[cluster_assignments[i]] for i in range(data.shape[0])])

        return new_labels

    def k_means(self, data):
        """
        Main K-Means algorithm that performs clustering of the data.
        
        Arguments: 
            data (array): shape (N,D) where N is the number of data samples, D is number of features.
            max_iter (int): the maximum number of iterations
        Returns:
            centers (array): shape (K,D), the final cluster centers.
            cluster_assignments (array): shape (N,) final cluster assignment for each data point.
        """
        ##
        ###
        #### WRITE YOUR CODE HERE!
        centers = self.init_centers(data)

        # Loop over the iterations
        for i in range(self.max_iters):
            old_centers = centers.copy()  # keep in memory the centers of the previous iteration

            distances = self.compute_distance(data, centers)
            cluster_assignments = self.find_closest_cluster(distances)
            centers = self.compute_centers(data, cluster_assignments)

            if np.all(old_centers == centers):
                break

        # Compute the final cluster assignments
        distances = self.compute_distance(data, centers)
        cluster_assignments = self.find_closest_cluster(distances)
        self.centers = centers
        ###
        ##
        return centers, cluster_assignments

    def fit(self, training_data, training_labels):
        """
        Train the model and return predicted labels for training data.

        You will need to first find the clusters by applying K-means to
        the data, then to attribute a label to each cluster based on the labels.
        
        Arguments:
            training_data (array): training data of shape (N,D)
            training_labels (array): labels of shape (N,)
        Returns:
            pred_labels (array): labels of shape (N,)
        """
        ##
        ###
        #### WRITE YOUR CODE HERE!
        self.centers_labels = self.assign_labels_to_center(*self.k_means(training_data), training_labels)
        ###
        ##
        return self.predict(training_data)

    def predict(self, test_data):
        """
        Runs prediction on the test data given the cluster center and their labels.

        To do this, first assign data points to their closest cluster, then use the label
        of that cluster as prediction.
        
        Arguments:
            test_data (np.array): test data of shape (N,D)
        Returns:
            pred_labels (np.array): labels of shape (N,)
        """
        ##
        ###
        #### WRITE YOUR CODE HERE!
        pred_labels = self.predict_with_centers(test_data, self.centers, self.centers_labels)
        ###
        ##
        return pred_labels
