import math

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader

from ..utils import onehot_to_label, CUDA


## MS2

class MLP(nn.Module):
    """
    An MLP network which does classification.

    It should not use any convolutional layers.
    """

    def __init__(self, input_size, n_classes):
        """
        Initialize the network.
        
        You can add arguments if you want, but WITH a default value, e.g.:
            __init__(self, input_size, n_classes, my_arg=32)
        
        Arguments:
            input_size (int): size of the input
            n_classes (int): number of classes to predict
        """
        super(MLP, self).__init__()

        # Fully connected layers
        # After testing for 1, 2 and 3 layers with various sizes, we decided this configuration was the best one
        self.fc1 = nn.Linear(input_size, 522)  # 522 = (1024 + 20) / 2
        self.fc2 = nn.Linear(522, 271)  # 271 = (522 + 20) / 2
        self.fc3 = nn.Linear(271, n_classes)

    def forward(self, x):
        """
        Predict the class of a batch of samples with the model.

        Arguments:
            x (tensor): input batch of shape (N, D)
        Returns:
            preds (tensor): logits of predictions of shape (N, C)
                Reminder: logits are value pre-softmax.
        """
        # ensure x is on the right device
        x = CUDA.convert(x)

        # ReLu is the best activation function in our case
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        preds = self.fc3(x)

        return preds


class CNN(nn.Module):
    """
    A CNN which does classification.

    It should use at least one convolutional layer.
    """

    def __init__(self, input_channels, n_classes, img_dim=32):
        """
        Initialize the network.
        
        You can add arguments if you want, but WITH a default value, e.g.:
            __init__(self, input_channels, n_classes, my_arg=32)
        
        Arguments:
            input_channels (int): number of channels in the input
            n_classes (int): number of classes to predict
        """
        super(CNN, self).__init__()

        # Convolutional layer
        # 2 convolutional layer seems to be the best (1 is too little and 3 too much)
        self.conv1 = nn.Conv2d(input_channels, 32, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3, padding=1)

        # Max pooling layer
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        # Fully connected layers
        # More layers makes computation very slow and less accurate
        self.fc1 = nn.Linear(int(64 * np.power(int(img_dim / 4), 2)), 522)  # again, 522 = (1024 + 20) / 2
        self.fc2 = nn.Linear(522, n_classes)

    def forward(self, x):
        """
        Predict the class of a batch of samples with the model.

        Arguments:
            x (tensor): input batch of shape (N, Ch, H, W)
        Returns:
            preds (tensor): logits of predictions of shape (N, C)
                Reminder: logits are value pre-softmax.
        """
        # ensure x is on the right device
        x = CUDA.convert(x)

        # Convolutional layer
        x = self.conv1(x)
        x = F.relu(x)

        # Max pooling layer
        x = self.pool1(x)

        # Second Layer
        x = self.conv2(x)
        x = F.relu(x)
        x = self.pool2(x)

        # Flatten the tensor for fully connected layers
        x = x.view(x.size(0), -1)

        # Fully connected layers
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)

        return x


class Trainer(object):
    """
    Trainer class for the deep networks.

    It will also serve as an interface between numpy and pytorch.
    """

    def __init__(self, model, lr, epochs, batch_size, silent=False):
        """
        Initialize the trainer object for a given model.

        Arguments:
            model (nn.Module): the model to train
            lr (float): learning rate for the optimizer
            epochs (int): number of epochs of training
            batch_size (int): number of data points in each batch
        """
        self.lr = lr
        self.epochs = epochs
        self.model = CUDA.convert(model)  # ensure model is on the right device
        self.batch_size = batch_size

        self.criterion = CUDA.convert(nn.CrossEntropyLoss())  # ensure criterion is on the right device
        self.optimizer = torch.optim.SGD(self.model.parameters(), self.lr)

        self.silent = silent
        self.ep = 0

    # prints progress when not silenced by main
    def print_progress(self, it):
        if not self.silent:
            iteration_str = f"Ieration {it}".ljust(14)
            epoch_str = f" of Epoch {self.ep}".ljust(22)
            print(iteration_str + epoch_str, end='\r', flush=True)

    def train_all(self, dataloader):
        """
        Fully train the model over the epochs. 
        
        In each epoch, it calls the functions "train_one_epoch". If you want to
        add something else at each epoch, you can do it here.

        Arguments:
            dataloader (DataLoader): dataloader for training data
        """
        for ep in range(self.epochs):
            self.train_one_epoch(dataloader)

            self.ep = ep

        # once done, prints the number of the last epoch (max_iters - 1)
        if not self.silent:
            print(f"Done for last epoch ({ep})".ljust(30))

    def train_one_epoch(self, dataloader):
        """
        Train the model for ONE epoch.

        Should loop over the batches in the dataloader. (Recall the exercise session!)
        Don't forget to set your model to training mode, i.e., self.model.train()!

        Arguments:
            dataloader (DataLoader): dataloader for training data
        """
        self.model.train()
        for it, batch in enumerate(dataloader):
            x, y = CUDA.convert(batch)
            y = y.long()

            logits = self.model.forward(x)

            loss = self.criterion(logits, y)

            loss.backward()

            self.optimizer.step()

            self.optimizer.zero_grad()

            self.print_progress(it)

    def predict_torch(self, dataloader: DataLoader):
        """
        Predict the validation/test dataloader labels using the model.

        Hints:
            1. Don't forget to set your model to eval mode, i.e., self.model.eval()!
            2. You can use torch.no_grad() to turn off gradient computation, 
            which can save memory and speed up computation. Simply write:
                with torch.no_grad():
                    # Write your code here.

        Arguments:
            dataloader (DataLoader): dataloader for validation/test data
        Returns:
            pred_labels (torch.tensor): predicted labels of shape (N,),
                with N the number of data points in the validation/test data.
        """
        pred_labels = []
        self.model.eval()
        with torch.no_grad():
            for it, batch in enumerate(dataloader):
                x, = CUDA.convert(batch)
                pred_labels.append(onehot_to_label(self.model(x).cpu()))
        pred_labels = torch.cat(pred_labels, dim=0)

        return pred_labels

    def fit(self, training_data, training_labels):
        """
        Trains the model, returns predicted labels for training data.

        This serves as an interface between numpy and pytorch.

        Arguments:
            training_data (array): training data of shape (N,D)
            training_labels (array): regression target of shape (N,)
        Returns:
            pred_labels (array): target of shape (N,)
        """
        # First, prepare data for pytorch
        train_dataset = TensorDataset(torch.from_numpy(training_data).float(),
                                      torch.from_numpy(training_labels))
        train_dataloader = DataLoader(train_dataset, batch_size=self.batch_size, shuffle=True)

        self.train_all(train_dataloader)

        return self.predict(training_data)

    def predict(self, test_data):
        """
        Runs prediction on the test data.

        This serves as an interface between numpy and pytorch.
        
        Arguments:
            test_data (array): test data of shape (N,D)
        Returns:
            pred_labels (array): labels of shape (N,)
        """
        # First, prepare data for pytorch
        test_dataset = TensorDataset(torch.from_numpy(test_data).float())
        test_dataloader = DataLoader(test_dataset, batch_size=self.batch_size, shuffle=False)

        pred_labels = self.predict_torch(test_dataloader)

        # We return the labels after transforming them into numpy array.
        return pred_labels.cpu().numpy()
