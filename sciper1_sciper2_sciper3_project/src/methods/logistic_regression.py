import numpy as np


class LogisticRegression(object):
    """
    Logistic regression classifier.
    """

    def __init__(self, lr, max_iters=500):
        """
        Initialize the new object (see dummy_methods.py)
        and set its arguments.

        Arguments:
            lr (float): learning rate of the gradient descent
            max_iters (int): maximum number of iterations
        """
        self.lr = lr
        self.max_iters = max_iters
        self.weights = None

    def accuracy_fn(self, labels_pred, labels_gt):
        return np.sum(np.equal(labels_pred, labels_gt)) / labels_pred.shape[0] * 100

    def f_softmax(self, data, W):
        return np.array([np.exp(data[i].T @ W) / np.sum(np.exp(data[i].T @ W)) for i in range(data.shape[0])])

    def loss_logistic_multi(self, data, labels, w):
        return - np.sum(np.sum(labels * np.log(self.f_softmax(data, w))))

    def gradient_logistic_multi(self, data, labels, W):
        return data.T @ (self.f_softmax(data, W) - labels)

    def logistic_regression_predict_multi(self, data, W):
        return np.argmax(self.f_softmax(data, W), axis=1)

    def onehot_to_label(self, onehot):
        return np.argmax(onehot, axis=1)

    def label_to_onehot(self, label):
        one_hot_labels = np.zeros([label.shape[0], int(np.max(label) + 1)])
        one_hot_labels[np.arange(label.shape[0]), label.astype(int)] = 1
        return one_hot_labels

    def fit(self, training_data, training_labels):
        """
        Trains the model, returns predicted labels for training data.

        Arguments:
            training_data (array): training data of shape (N,D)
            training_labels (array): regression target of shape (N,)
        Returns:
            pred_labels (array): target of shape (N,)
        """
        ##
        ###
        #### WRITE YOUR CODE HERE!
        training_labels_onehot = self.label_to_onehot(training_labels)

        D = training_data.shape[1]  # number of features
        C = training_labels_onehot.shape[1]
        # Random initialization of the weights
        weights = np.random.normal(0, 0.1, (D, C))
        for it in range(self.max_iters):
            gradient = self.gradient_logistic_multi(training_data, training_labels_onehot, weights)
            weights = weights - self.lr * gradient

            predictions = self.logistic_regression_predict_multi(training_data, weights)
            if self.accuracy_fn(predictions, training_labels) == 100:
                break
        self.weights = weights
        ###
        ##
        return self.predict(training_data)

    def predict(self, test_data):
        """
        Runs prediction on the test data.
        
        Arguments:
            test_data (array): test data of shape (N,D)
        Returns:
            pred_labels (array): labels of shape (N,)
        """
        ##
        ###
        #### WRITE YOUR CODE HERE!
        pred_labels = self.logistic_regression_predict_multi(test_data, self.weights)
        ###
        ##
        return pred_labels