import argparse

import numpy as np
from torchinfo import summary

from src.methods.pca import PCA
from src.methods.deep_network import MLP, CNN, Trainer

from src.data import load_data
from src.methods.dummy_methods import DummyClassifier
from src.methods.kmeans import KMeans
from src.methods.logistic_regression import LogisticRegression
from src.methods.svm import SVM
from src.utils import normalize_fn, accuracy_fn, macrof1_fn, get_n_classes, CUDA

import time


def main(args):
    """
    The main function of the script. Do not hesitate to play with it
    and add your own code, visualization, prints, etc!

    Arguments:
        args (Namespace): arguments that were parsed from the command line (see at the end 
                          of this file). Their value can be accessed as "args.argument".
    """
    ## 1. First, we load our data and flatten the images into vectors
    xtrain, xtest, ytrain, ytest = load_data(args.data)
    xtrain = xtrain.reshape(xtrain.shape[0], -1)
    xtest = xtest.reshape(xtest.shape[0], -1)


    ## 2. Then we must prepare it. This is were you can create a validation set,
    #  normalize, add bias, etc.
    mean = np.mean(xtrain, axis=0)
    std = np.std(xtrain, axis=0)
    xtrain = normalize_fn(xtrain, mean, std)
    xtest = normalize_fn(xtest, mean, std)

    # Make a validation set
    if not args.test:
        num_samples = xtrain.shape[0]
        fraction_train = 0.8  # 80% of data is reserved for training, so 20% for testing
        rinds = np.random.permutation(num_samples)

        n_train = int(num_samples * fraction_train)
        xtest = xtrain[rinds[n_train:]]
        ytest = ytrain[rinds[n_train:]]

        xtrain = xtrain[rinds[:n_train]]
        ytrain = ytrain[rinds[:n_train]]

    # Start timer
    if args.file_data or args.time:
            s1 = time.time()

    # Dimensionality reduction (MS2)
    if args.use_pca:
        pca_obj = PCA(d=args.pca_d)

        exvar = pca_obj.find_principal_components(xtrain)
        xtrain = pca_obj.reduce_dimension(xtrain)
        xtest = pca_obj.reduce_dimension(xtest)
        if not args.return_results:
            print(f'Using PCA, the total variance explained by the first {args.pca_d} principal components is {exvar:.3f} %')

    ## 3. Initialize the method you want to use.

    # Neural Networks (MS2)
    if args.method == "nn":
        # Sends the neural network to the GPU if it is available and no_gpu is not enabled
        CUDA.to_gpu(args.no_gpu)

        if not args.return_results:
            print("Using deep network")

        # Prepare the model (and data) for Pytorch
        # Note: you might need to reshape the image data depending on the network you use!
        n_classes = get_n_classes(ytrain)
        if args.nn_type == "mlp":
            model = MLP(input_size=xtrain.shape[1], n_classes=n_classes)

        elif args.nn_type == "cnn":
            # In order to work with CNN, we must reshape the input data
            xtrain = xtrain.reshape(xtrain.shape[0], 1, int(np.sqrt(xtrain.shape[1])), int(np.sqrt(xtrain.shape[1])))
            xtest = xtest.reshape(xtest.shape[0], 1, int(np.sqrt(xtest.shape[1])), int(np.sqrt(xtest.shape[1])))
            model = CNN(1, n_classes=n_classes, img_dim=xtrain.shape[3])

        if not args.return_results:
            summary(model)
            print('Working on cpu' if CUDA.cpu else 'Working on gpu')

        # Trainer object
        method_obj = Trainer(model,
                             lr=args.lr,
                             epochs=args.max_iters,
                             batch_size=args.nn_batch_size,
                             silent=args.return_results)
    
    # Follow the "DummyClassifier" example for your methods (MS1)
    elif args.method == "dummy_classifier":
        method_obj = DummyClassifier(arg1=1, arg2=2)

    elif args.method == "kmeans":
        method_obj = KMeans(args.K, args.max_iters)

    elif args.method == "logistic_regression":
        method_obj = LogisticRegression(args.lr, args.max_iters)

    elif args.method == "svm":
        method_obj = SVM(args.svm_c, args.svm_kernel, args.svm_gamma, args.svm_degree, args.svm_coef0)

    ## 4. Train and evaluate the method

    # Fit (:=train) the method on the training data
    preds_train = method_obj.fit(xtrain, ytrain)
        
    # Predict on unseen data
    preds = method_obj.predict(xtest)

    ## Report results: performance on train and valid/test sets
    acc = accuracy_fn(preds_train, ytrain)
    macrof1 = macrof1_fn(preds_train, ytrain)

    acc_test = accuracy_fn(preds, ytest)
    macrof1_test = macrof1_fn(preds, ytest)

    # Return the results alongside the time and the exvar if using PCA
    if args.return_results:
        return \
            (acc, macrof1), \
            (acc_test, macrof1_test), \
            time.time() - s1 if args.time else None, \
            exvar if args.use_pca else None

    #
    print(f"\nTrain set: accuracy = {acc:.3f}% - F1-score = {macrof1:.6f}")
    print(f"Test set:  accuracy = {acc_test:.3f}% - F1-score = {macrof1_test:.6f}")

    if args.time:
        print(args.method + f" takes {time.time() - s1:.2f} " + f"seconds {'with' if args.use_pca else 'without'} PCA")

    # Writes the results to a file for later use (only with cnn and mlp)
    if args.file_data:
        if args.nn_type == "cnn":
            f = open("cnn_stats.txt", "a")
        if args.nn_type == "mlp":
            f = open("mlp_stats.txt", "a")
        
        if args.nn_type == "cnn" or args.nn_type == "mlp":

            f.write(f"Train set: accuracy = {acc:.3f}% - F1-score = {macrof1:.6f}")
            f.write(f"\nTest set:  accuracy = {acc_test:3f}% - F1-score = {macrof1_test:.6f}")
            f.write(args.nn_type + " with parameter :\n" )
            f.write(f"lr : {args.lr}\n")
            f.write(f"batch_size : {args.nn_batch_size}\n")

            s2 = time.time()
            s3 = s2-s1
            f.write(args.method + f" takes {s3:.2f} " + f"seconds {'with' if args.use_pca else 'without'} PCA \n\n")

            f.close()



if __name__ == '__main__':
    # Definition of the arguments that can be given through the command line (terminal).
    # If an argument is not given, it will take its default value as defined below.
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', default="dataset_HASYv2", type=str, help="the path to wherever you put the data, if it's in the parent folder, you can use ../python main2.py --data C:\Cours\BA4\ML\MS2\ms2\sciper1_sciper2_sciper3_project\dataset_MS2 --method nn --nn_type cnn --lr 1e-5 --max_iters 100_HASYv2")
    parser.add_argument('--method', default="dummy_classifier", type=str, help="dummy_classifier / kmeans / logistic_regression / svm / nn (MS2)")
    parser.add_argument('--K', type=int, default=10, help="number of clusters for K-Means")
    parser.add_argument('--lr', type=float, default=1e-5, help="learning rate for methods with learning rate")
    parser.add_argument('--max_iters', type=int, default=100, help="max iters for methods which are iterative")
    parser.add_argument('--test', action="store_true", help="train on whole training data and evaluate on the test data, otherwise use a validation set")
    parser.add_argument('--svm_c', type=float, default=1., help="Constant C in SVM method")
    parser.add_argument('--svm_kernel', default="linear", help="kernel in SVM method, can be 'linear' or 'rbf' or 'poly'(polynomial)")
    parser.add_argument('--svm_gamma', type=float, default=1., help="gamma prameter in rbf/polynomial SVM method")
    parser.add_argument('--svm_degree', type=int, default=1, help="degree in polynomial SVM method")
    parser.add_argument('--svm_coef0', type=float, default=0., help="coef0 in polynomial SVM method")

    ### WRITE YOUR CODE HERE: feel free to add more arguments here if you need!
    parser.add_argument('--return_results', action='store_true', help="returns the accuracy for further analysis (suppress prints)")
    parser.add_argument('--time', action='store_true', help="returns/prints the time it took to do the calculation")
    parser.add_argument('--no_gpu', action='store_true', help='forces computation on cpu only (for nn)')
    parser.add_argument('--file_data', action='store_true', help="stores results into a file (mlp and cnn only)")

    # MS2 arguments
    parser.add_argument('--use_pca', action="store_true", help="to enable PCA")
    parser.add_argument('--pca_d', type=int, default=200, help="output dimensionality after PCA")
    parser.add_argument('--nn_type', default="mlp", help="which network to use, can be 'mlp' or 'cnn'")
    parser.add_argument('--nn_batch_size', type=int, default=64, help="batch size for NN training")

    # "args" will keep in memory the arguments and their values,
    # which can be accessed as "args.data", for example.
    args = parser.parse_args()
    main(args)
