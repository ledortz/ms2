import main
import Plotting

list = [1, 0.5, 0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001, 0.00001]

for a in list:
    for batch in range(500,5000,500):
        args = Plotting.DummyArgParser(data='C:\\Cours\\BA4\\ML\\MS2\\ms2\\sciper1_sciper2_sciper3_project\\dataset_MS2', method='nn', K=10, lr=a, max_iters=100, test=True, svm_c=1.0, svm_kernel='linear', svm_gamma=1.0, svm_degree=1, svm_coef0=0.0, return_results=False, file_data=True, use_pca=False, pca_d=200, nn_type='cnn', nn_batch_size=batch)
        main.main(args)

for a in list:
    for batch in range(500,5000,500):
        args = Plotting.DummyArgParser(data='C:\\Cours\\BA4\\ML\\MS2\\ms2\\sciper1_sciper2_sciper3_project\\dataset_MS2', method='nn', K=10, lr=a, max_iters=100, test=True, svm_c=1.0, svm_kernel='linear', svm_gamma=1.0, svm_degree=1, svm_coef0=0.0, return_results=False, file_data=True, use_pca=False, pca_d=200, nn_type='mlp', nn_batch_size=batch)
        main.main(args)
