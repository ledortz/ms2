import argparse
import ctypes

import importlib
import signal
import sys
from multiprocessing import Queue, Process, Manager, Pool
import os
import json

import numpy as np
import win32api
from matplotlib import pyplot as plt

os.environ['KMP_DUPLICATE_LIB_OK'] = 'TRUE'


class DummyArgParser:
    def __init__(self,
                 data="./dataset_MS2/",
                 method='nn',
                 K=10,
                 lr=1e-5,
                 max_iters=100,
                 test=True,
                 svm_c=1.,
                 svm_kernel='linear',
                 svm_gamma=1.,
                 svm_degree=1,
                 svm_coef0=0.,
                 use_pca=False,
                 pca_d=200,
                 nn_type='mlp',
                 nn_batch_size=64,
                 return_results=True,
                 time=False,
                 no_gpu=False,
                 file_data=False):
        self.data = data
        self.method = method
        self.K = K
        self.lr = lr
        self.max_iters = max_iters
        self.test = test
        self.svm_c = svm_c
        self.svm_kernel = svm_kernel
        self.svm_gamma = svm_gamma
        self.svm_degree = svm_degree
        self.svm_coef0 = svm_coef0
        self.use_pca = use_pca
        self.pca_d = pca_d
        self.nn_type = nn_type
        self.nn_batch_size = nn_batch_size
        self.return_results = return_results
        self.time = time
        self.no_gpu = no_gpu
        self.file_data = file_data


class PCA:
    def __init__(self, threads=4):
        self.threads = threads
        self.data = {}

    def compute(self, arg: DummyArgParser, id):
        import main
        print(f'Computing {id}')
        result_pca = main.main(arg)
        print(f'Done {id}')

        return result_pca, id

    def prepare_data(self):
        if not os.path.isfile('./Plotting_data_pca.json'):
            ds = np.concatenate((np.arange(10, 100, 10),
                                 np.arange(100, 1000, 50),
                                 np.arange(1000, 32 * 32, 500)))
            self.data = {'input': ds.tolist(),
                         'output': {'pca':
                                        {'time': {}, 'acc': {}, 'f1': {}, 'exvar': {}},
                                    'no_pca':
                                        {'time': None, 'acc': None, 'f1': None}}}
            with open('./Plotting_data_pca.json', 'w') as fp:
                json.dump(self.data, fp, indent=4, separators=(',', ': '))
        else:
            with open('./Plotting_data_pca.json', 'r') as fp:
                self.data = json.load(fp)

    def main(self):
        self.prepare_data()
        args = [(DummyArgParser(method='kmeans', K=100, max_iters=50, time=True, use_pca=True, pca_d=d), i)
                for i, d in enumerate(self.data['input'])]
        with Pool(self.threads) as pool:
            results = pool.starmap(self.compute, args)

        for result_pca, id in results:
            self.data['output']['pca']['acc'][id] = result_pca[1][0]
            self.data['output']['pca']['f1'][id] = result_pca[1][1]
            self.data['output']['pca']['time'][id] = result_pca[2]
            self.data['output']['pca']['exvar'][id] = result_pca[3]

        kmeans = self.compute(DummyArgParser(method='kmeans', K=100, max_iters=50, time=True), 0)[0]
        self.data['output']['no_pca']['acc'] = kmeans[1][0]
        self.data['output']['no_pca']['f1'] = kmeans[1][1]
        self.data['output']['no_pca']['time'] = kmeans[2]
        self.data['output']['no_pca']['exvar'] = kmeans[3]

        with open('Plotting_data_pca.json', 'w') as fp:
            json.dump(self.data, fp, indent=4, separators=(',', ': '))

    @staticmethod
    def plot_from_data(hehe):
        with open('./Plotting_data_pca.json', 'r') as fp:
            data = json.load(fp)

        x = data['input']

        y1_pca = list(data['output']['pca']['acc'].values())
        y2_pca = list(data['output']['pca']['f1'].values())
        y3_pca = list(data['output']['pca']['time'].values())
        y4_pca = list(data['output']['pca']['exvar'].values())

        y1 = np.zeros((len(x),)) + data['output']['no_pca']['acc']
        y2 = np.zeros((len(x),)) + data['output']['no_pca']['f1']
        y3 = np.zeros((len(x),)) + data['output']['no_pca']['time']

        _x = range(len(x))
        plt.plot(x, y1_pca, label='with pca')
        plt.plot(x, y1, label='without pca')
        plt.ylabel('accuracy in %')
        plt.xlabel('d')
        plt.title('accuracy of KNN')
        plt.legend()
        plt.show()

        plt.plot(x, y2_pca, label='with pca')
        plt.plot(x, y2, label='without pca')
        plt.ylabel('f1 score')
        plt.xlabel('d')
        plt.title('f1 score of KNN')
        plt.legend()
        plt.show()

        plt.plot(x, y3_pca, label='with pca')
        plt.plot(x, y3, label='without pca')
        plt.ylabel('time in s')
        plt.xlabel('d')
        plt.title('time of KNN')
        plt.legend()
        plt.show()

        threshold = np.zeros((len(x),)) + 90
        plt.plot(x, y4_pca)
        plt.plot(x, threshold)
        plt.ylabel('total variance in %')
        plt.xlabel('d')
        plt.title('total variance of KNN using PCA')
        plt.show()


class SVM:
    def __init__(self, method='mlp', threads=4):
        self.data = {}
        self.todo = 0
        self.method = method
        self.threads = threads
        self.queue = None

    def fin(self):
        self.data['status'] = self.todo
        with open("./Plotting_data_" + self.method + ".json", 'w') as fp:
            json.dump(self.data, fp, indent=4, separators=(',', ': '))
            print(f'\nStopped at {self.todo}'.ljust(20))
            print('Saved progress')

    def compute(self):
        import main

        args = self.data['input'][str(self.todo)]
        arg = DummyArgParser(lr=args[0], nn_batch_size=args[1], max_iters=args[2], nn_type=self.method)
        self.queue.put(main.main(arg)[1])

    def main(self):

        if not os.path.isfile("./Plotting_data_" + self.method + ".json"):
            lrs = (np.arange(1e-7, 1e-6, 2e-7),
                   np.arange(1e-6, 1e-5, 2e-6),
                   np.arange(1e-5, 1e-4, 2e-5),
                   np.arange(1e-4, 1e-3, 2e-4),
                   np.arange(1e-3, 1e-2, 2e-3),
                   np.arange(1e-2, 1e-1, 2e-2),
                   np.arange(1e-1, 1, 2e-1))
            learning_rates = np.concatenate(lrs)
            bss = (np.arange(1, 10, 1),
                   np.arange(10, 100, 10),
                   np.arange(100, 1000, 100),
                   np.arange(1000, 10000, 1000))
            batch_sizes = np.concatenate(bss)

            args = {'status': 0, 'input': {}, 'lrs': [], 'bss': [], 'output': {'acc': {}, 'f1': {}}}
            id = 0
            for lr in learning_rates:
                for bs in batch_sizes:
                    args['input'][id] = [lr, int(bs), 100]
                    id += 1
            args['lrs'] = learning_rates.tolist()
            args['bss'] = batch_sizes.tolist()
            args['last'] = id - 1

            with open("./Plotting_data_" + self.method + ".json", 'w') as fp:
                json.dump(args, fp, sort_keys=True, indent=4, separators=(',', ': '))

        with open("./Plotting_data_" + self.method + ".json", 'r') as fp:
            self.data = json.load(fp)

        learning_rates = np.asarray(self.data['lrs'])
        batch_sizes = np.asarray(self.data['bss'])
        last = self.data['last']
        self.todo = self.data['status']
        print(f'Taking back at {self.todo}')
        sys.stdout.write(f'\rComputing {self.todo} - {min(self.todo + self.threads, last)}')
        sys.stdout.flush()
        try:
            while True:
                self.queue = Queue()

                processes = []
                for i in range(self.todo, min(self.todo + self.threads, last + 1)):
                    p = Process(target=self.compute)
                    p.start()
                    processes.append(p)

                f = 0
                for i in range(self.todo, min(self.todo + self.threads, last + 1)):
                    processes[f]
                    self.data['output']['acc'][str(i)], self.data['output']['f1'][str(i)] = self.queue.get()
                    f += 1

                    # breaks once all the computation has been done
                if self.todo > last - self.threads:
                    break

                self.todo += self.threads
                sys.stdout.write(f'\rComputing {self.todo} - {self.todo + self.threads}')
                sys.stdout.flush()

            # Once done, store the results in the json file
            with open("./Plotting_data_" + self.method + ".json", 'w') as fp:
                json.dump(self.data, fp, indent=4, separators=(',', ': '))

            # Plot the results once finished
            X, Y = np.meshgrid(range(len(learning_rates)), range(len(batch_sizes)))

            Z = np.array(list(self.data['output']['acc'].values())).reshape(len(learning_rates), len(batch_sizes)).T
            plot(X, Y, Z, learning_rates, batch_sizes, f'Accuracy for {self.method}', 'accuracy in %')
            Z = np.array(list(self.data['output']['f1'].values())).reshape(len(learning_rates), len(batch_sizes)).T
            plot(X, Y, Z, learning_rates, batch_sizes, f'F1 score for {self.method}', 'f1 score')

        # If interrupting, stores the results and where to continue
        finally:
            self.fin()

    @staticmethod
    def plot_from_data(method='mlp'):
        with open('./Plotting_data_' + method + '.json', 'r') as fp:
            data = json.load(fp)

        x = data['lrs']
        y = data['bss']
        X, Y = np.meshgrid(range(len(x)), range(len(y)))

        Z = np.array(list(data['output']['acc'].values())).reshape(len(x), len(y)).T
        Z2 = np.array(list(data['output']['f1'].values())).reshape(len(x), len(y)).T

        plot(X, Y, Z, x, y, f'Accuracy for {method}', 'accuracy in %')
        plot(X, Y, Z2, x, y, f'F1 score for {method}', 'f1 score in %')


def plot(X, Y, Z, learning_rates, batch_sizes, title, label):
    plt.pcolormesh(X, Y, Z, cmap='hot')
    plt.xticks(range(len(learning_rates)), learning_rates)
    plt.yticks(range(len(batch_sizes)), batch_sizes)

    plt.ylabel('batch sizes')
    plt.xlabel('learning rates')

    cbar = plt.colorbar()
    cbar.set_label(label, rotation=270, labelpad=20)

    plt.title(title)

    plt.show()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('-c', action='store_true')
    parser.add_argument('-t', type=int, default=1)
    parser.add_argument('-p', action='store_true')
    parser.add_argument('-m', type=str, default='cnn')

    args = parser.parse_args()

    if args.m in ['cnn', 'mlp']:
        method = SVM(method=args.m, threads=args.t)
    elif args.m == 'kmeans':
        method = PCA(threads=args.t)

    if args.c:
        def sigint_handler(signal, frame):
            sys.exit(0)


        signal.signal(signal.SIGINT, sigint_handler)

        method.main()

    if args.p:
        method.plot_from_data(args.m)
